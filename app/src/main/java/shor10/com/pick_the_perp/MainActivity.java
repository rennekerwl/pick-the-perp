package shor10.com.pick_the_perp;

        import android.graphics.drawable.Drawable;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;

        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.android.volley.toolbox.StringRequest;
        import com.squareup.picasso.Picasso;

        import org.jsoup.Jsoup;
        import org.jsoup.nodes.Document;
        import org.jsoup.nodes.Element;
        import org.jsoup.select.Elements;

        import java.io.IOException;
        import java.util.Random;

        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.JsonObjectRequest;
        import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {

    Button getBtn;
    TextView result;
    ImageView testImage;  //test image view is used for testing loading image from URL
    String testImageURL = "https://florida.arrests.org/mugs/Palmbeach/2018/2018008309.jpg";  //URL of image to load for testing


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        testImage = (ImageView) findViewById(R.id.imageView);
        result = (TextView) findViewById(R.id.result);
        getBtn = (Button) findViewById(R.id.getBtn);
        getBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWebsite();
            }
        });
    }

    //takes a URL to an image and and imageview
    //places url image in imageview
    private void loadImageFromUrl(String url, ImageView view){
        Picasso.with(this).load(url).placeholder(R.mipmap.ic_launcher) //sets placeholder image if you want one
        .error(R.mipmap.ic_launcher) //if there is an error load this image
        .into(view, new com.squareup.picasso.Callback(){


            public void onSuccess(){
                //do nothing
            }

            public void onError(){
                //do nothing
            }
        });
    }

    public int getRandomNumber(int bind){
        Random rand = new Random();
        return rand.nextInt(bind);
    }

    private String getPerpInfo(String PerpURL) {
        //This function doesn't work.
        String mugshotURL = "";
        final String PerpURL2 = PerpURL;
        new Thread(new Runnable() {
            @Override
            public void run() {
                final StringBuilder builder = new StringBuilder();
                final StringBuilder imageURL = new StringBuilder();

                try {
                    Document doc = Jsoup.connect(PerpURL2).get();
//
                    //Get the links from the first table on the page
                    Elements images = doc.getElementsByTag("img");
                    Elements paragraphs = doc.getElementsByTag("p");

                    int size = images.size();

//                    builder.append(images.get(3).absUrl("src"));

                    String mugshotURL = images.get(3).absUrl("src");
//                    return mugshotURL;

                    int cardNumber = getRandomNumber(100);


                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        result.setText(builder.toString());
//                        loadImageFromUrl(testImageURL.toString(), testImage);
                        Log.d("DEBUG: ", testImageURL.toString());
                    }
                });
            }

        }).start();
        Log.d("Outside if statement Image URL", mugshotURL);
        if (mugshotURL != "") {
            Log.d("If Statement Image URL", mugshotURL);
            return mugshotURL;
        }
        else{
            return "https://florida.arrests.org/mugs/Palmbeach/2018/2018008309.jpg";
        }
    }


    public void getWebsite() {
        //Right now this function shows how many perps are on page 1.


        final TextView mTextView = (TextView) findViewById(R.id.text);
// ...

// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://www.google.com";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        result.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.setText("That didn't work!");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);







//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                final StringBuilder builder = new StringBuilder();
//                final StringBuilder imageURL = new StringBuilder();
//                String PerpURLTest = "";
//
//                try {
//                    Document doc = Jsoup.connect("http://www.orangecountymugshots.com/mugshots-all.php").get();
//
//                    //Get the links from the first table on the page
//                    Elements table = doc.getElementsByTag("table");
//                    Elements links = table.get(0).getElementsByTag("a");
//
//                    int size = links.size();
//
//                    builder.append(size);
//
//                    PerpURLTest = links.get(0).absUrl("href");
//                    Log.d("Perp Page URL: ", PerpURLTest);
//                    imageURL.append(PerpURLTest);
//
//
//                    int cardNumber = getRandomNumber(100);
//
////                    builder.append(cards.get(cardNumber).getElementsByClass("title").text());
////                    builder.append("\n");
////                    builder.append(cards.get(cardNumber).getElementsByClass("charge-title").text());
////                    builder.append("\n");
////                    builder.append("https://wisconsin.arrests.org" + cards.get(cardNumber).getElementsByClass("placeholder").attr("data-large"));
////                    imageURL.append("https://wisconsin.arrests.org" + cards.get(cardNumber).getElementsByClass("placeholder").attr("data-large"));
//
//
//                } catch (IOException e) {
//                    builder.append("Error : ").append(e.getMessage()).append("\n");
//                }
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        result.setText(builder.toString());
////                        Log.d("Perp Image URL: ", getPerpInfo(imageURL.toString()));
////                        loadImageFromUrl(getPerpInfo(imageURL.toString()), testImage);
////                        Log.d("DEBUG: ", testImageURL.toString());
//                    }
//                });
//            }
//        }).start();
    }
}